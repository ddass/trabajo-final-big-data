# Trabajo Final Big Data
Trabajo final de big data del modulo "Open & Big Data"
## Uso
Para usar el script solo se tiene que ingresar las credenciales a la api de Twitter
## Algoritmo de analisis de datos
Para este trabajo se utilizo el algoritmo de VADER. El motivo principal por el cual se uso este algoritmo es que esta optimizado para analizar redes sociales.
## Preprocesamiento de datos
1. Se extrae las menciones de cada tweet y se guarda en otra columna
2. Se limpian los tweets. Se quitan los emojis, menciones, RTs, hashtags, hyperlinks, nuevas lineas "\n" y signos de puntuacion.
3. Se etiquetan los tokens y se quitan los stopwords.
4. Se reduce las palabras a su raiz.
5. Se calcula en sentimiento (-1, 0, 1) y el analisis (negativo, neutro, positivo).
6. Se remueven los tweets sin menciones.
7. Se guarda el analisis en un csv.
8. Se calculan los promedios de sentimiento
9. Se grafica.

